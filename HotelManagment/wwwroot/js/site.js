﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
$(function ($) {
    /*to top*/

    let top = $("div#toTopPage");
    toTopPage($, top);

    /*------*/
    /*fade between page*/
    (function ($) {
        $("div.container").hide().fadeIn('slow');
    })(jQuery);
    /*----*/

    tableOnSmallScreen();
    $(window).resize(function () {
        tableOnSmallScreen();
    });
    dateTimePickerSettings();
    mainSettings();
    tableFilter();

});

function toTopPage(window, top) {
    $(window).on('scroll', function () {
        if ($(this).scrollTop() >= 50) {
            top.fadeIn(100);
        }
        else {
            top.fadeOut('fast');
        }
        return false;
    });
    top.off('click').on('click', function () {
        $('html,body').animate({
            scrollTop: 0
        }, 1500);
        return false;
    });
    $('div#toTopPage>i').mouseover(function () {
        $(this).animate({
            opacity: 0.5
        }, 1500);
        return false;
    }).mouseleave(function () {
        $(this).animate({
            opacity: 0.6
        }, 300);
        return false;
    });
}

function mainSettings() {

    /*loader*/
    $('div.main-loader').fadeOut('slow');
    $("alert").fadeIn();
    $("div.alert>a.close").off('click').on('click', function () {
        $(this).parent().fadeOut('slow');
        return false;
    });
    /*---*/
}

function dateTimePickerSettings() {
    $('#fromDate,#toDate')
        .datetimepicker({
            format: 'd.m.y H:i',
            minDate: new Date()
        });
    $.datetimepicker.setLocale('pl');
}

function tableOnSmallScreen() {

    let thead = $('table.data-details>thead>tr>th');
    let tbody = $('table.data-details>tbody>tr');

    if (thead.length > 0 && tbody.length > 0) {
        $('div.main-loader').fadeIn('slow');
        $('.table').hide().fadeIn(() => {
            $('div.main-loader').fadeOut('slow');
        });

        if ($(this).width() <= 1200) {
            showHideDetailsTd(tbody, thead);
        } else {
            showHideDetailsTd(tbody, thead);
        }
    }

}


function showHideDetailsTd(tbody, thead) {
    for (let i = 0; i < tbody.length; i++) {
        let td = tbody.eq(i).children('td');
        for (let j = 0; j < td.length; j++) {
            let pom = td.eq(j).find('span');
            if (!pom.hasClass('content') && $(window).width() <= 1200) {
                td.eq(j).prepend(`
                    <span class="content">
                      <b> ${thead.eq(j).text().length > 0 ? thead.eq(j).text() + ':' : ''}  </b>
                    </span>`);
            } else {
                if (pom.hasClass('content') && $(window).width() > 1200) {
                    pom.remove('span.content');
                }
            }
        }
    }
}

function tableFilter() {
    $('#searchInput').on('keyup', function () {
        let val = $(this).val().toLowerCase();
        clearInputSearch(val);
        filter(val, 'tbody tr');
        return false;
    });

    function filter(val, nameElement) {
        $(nameElement).filter(function () {
            let isElement = $(this).text().toLowerCase().indexOf(val) > -1;
            if (!isElement) {
                $(this).fadeOut();
            }
            else {
                $(this).fadeIn();
            }
            $('div.main-loader').fadeIn(30).fadeOut(30);
        });
    }
}

function clearInputSearch(val) {
    let remove = $("div>i.fa");
    if (val.length > 0) {
        remove.css({ 'display': 'flex' });
        remove.off('click').on('click', function (e) {
            e.preventDefault();
            $('#searchInput').val('');
            remove.css({ 'display': 'none' });
            $('tbody tr').fadeIn();
        });
    }
    else {
        remove.css({ 'display': 'none' });
    }
}
