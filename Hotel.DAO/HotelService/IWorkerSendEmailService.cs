﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.DAO.HotelService
{
    public interface IWorkerSendEmailService
    {
        Task SendEmail(string from, string title, string descr);
        Task SendEmailToClients();
    }
}
